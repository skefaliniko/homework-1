//
//  HWHomeworkViewController.m
//  Homework1
//
//  Created by Vladislav Grigoriev on 20/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import "HWHomeworkViewController.h"
#import "HWHomeworkView.h"
#import "HWHomeworkModel.h"

#import "HWMatrixViewController.h"
#import "HWConverterViewController.h"
#import "HWAnimationViewController.h"

@interface HWHomeworkViewController () <HWMatrixViewControllerDelegate, HWConverterViewControllerDelegate, HWAnimationViewControllerDelegate, HWHomeworkViewDelegate>

@property (nonatomic, strong) HWHomeworkView *homeworkView;
@property (nonatomic, strong) HWHomeworkModel *homeworkModel;

@end

@implementation HWHomeworkViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _homeworkModel = [[HWHomeworkModel alloc] init];
    }
    return self;
}

- (void)loadView {
    self.homeworkView = [[HWHomeworkView alloc] init];
    [self setView:self.homeworkView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.homeworkView.delegate = self;
    
    [self.homeworkModel.controllerClasses enumerateObjectsUsingBlock:^(Class<HWHomeworkChildController>  _Nonnull controllerClass, NSUInteger idx, BOOL * _Nonnull stop) {
        UIViewController<HWHomeworkChildController> *controller = [[(Class)controllerClass alloc] init];
        controller.delegate = self;
        [self addChildViewController:controller];
        [self.homeworkView addView:controller.view];
        [controller didMoveToParentViewController:self];
    }];
}

#pragma mark - HWMatrixViewControllerDelegate

- (void)matrixController:(HWMatrixViewController *)controller needCreateMatrixWithRowsCount:(NSInteger)rowsCount columnsCount:(NSInteger)columnsCount {
    [self.homeworkModel createMatrixWithRowsCount:rowsCount
                                     columnsCount:columnsCount
                                       completion:^(NSArray<NSArray<NSNumber *> *> *matrix) {
                                           [controller setMatrix:matrix];
                                           [self.homeworkView setNeedsLayout];
                                       }];
}

#pragma mark - HWConverterViewControllerDelegate

- (void)converterController:(HWConverterViewController *)controller needConvertString:(NSString *)string {
    [self.homeworkModel convertString:string
                       withCompletion:^(NSString *convertedString) {
                           [controller setConvertedString:convertedString];
                           [self.homeworkView setNeedsLayout];
                       }];
}

#pragma mark - HWAnimationViewControllerDelegate

- (void)animationController:(HWAnimationViewController *)controller needPointForAnimationTime:(NSTimeInterval)time {
    [self.homeworkModel pointForTime:time
                                size:controller.view.bounds.size
                      withCompletion:^(CGPoint point) {
                          [controller addPoint:point];
                      }];
}

#pragma mark - HWHomeworkViewDelegate

- (void)homeworkViewShouldEndEditing:(HWHomeworkView *)view {
    [view endEditing:NO];
}

@end
