//
//  HWMatrixViewController.m
//  Homework1
//
//  Created by Vladislav Grigoriev on 29/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import "HWMatrixViewController.h"
#import "HWMatrixView.h"

@interface HWMatrixViewController () <UITextFieldDelegate>

@property (nonatomic, strong) HWMatrixView *matrixView;
@property (nonatomic, strong) NSCharacterSet *validCharacterSet;

@end

@implementation HWMatrixViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _validCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadView {
    self.matrixView = [[HWMatrixView alloc] init];
    self.view = self.matrixView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.matrixView.createButton.enabled = [self isInputValid];
    
    self.matrixView.rowsCountInputTextFiled.delegate = self;
    self.matrixView.columnsCountInputTextField.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldTextDidChangedNotificationHandler:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:self.matrixView.rowsCountInputTextFiled];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldTextDidChangedNotificationHandler:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:self.matrixView.columnsCountInputTextField];
    
    [self.matrixView.createButton addTarget:self
                                     action:@selector(createButtonEventHandler:)
                           forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Public methods

- (void)setMatrix:(NSArray<NSArray<NSNumber *> *> *)matrix {
    [self.matrixView setMatrix:matrix];
}

#pragma mark - Private methods

- (void)createButtonEventHandler:(UIButton *)button {
    if (self.delegate) {
        [self.delegate matrixController:self
          needCreateMatrixWithRowsCount:self.matrixView.rowsCountInputTextFiled.text.integerValue
                           columnsCount:self.matrixView.columnsCountInputTextField.text.integerValue];
    }
    [self.matrixView endEditing:NO];
}

- (void)endEditing {
    [self.matrixView endEditing:NO];
}

- (void)textFieldTextDidChangedNotificationHandler:(NSNotification *)notification {
    self.matrixView.createButton.enabled = [self isInputValid];
}

- (BOOL)isInputValid {
    return self.matrixView.rowsCountInputTextFiled.text.integerValue > 0 && self.matrixView.columnsCountInputTextField.text.integerValue > 0;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSCharacterSet *resultCharacterSet = [NSCharacterSet characterSetWithCharactersInString:resultString];
    return [self.validCharacterSet isSupersetOfSet:resultCharacterSet];
}


@end
