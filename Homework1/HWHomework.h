//
//  HWHomework.h
//  Homework1
//
//  Created by Vladislav Grigoriev on 21/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#ifndef HWHomework_h
#define HWHomework_h

#include <stdio.h>
#include <float.h>

typedef struct {
    double x;
    double y;
} HWPoint;

char* stringForNumberString(const char *numberString);

long** createMatrix(const long rows, const long columns);

HWPoint pointForAnimationTime(const double time, const double canvasSize);

#endif /* HWHomework_h */
